+++
title = "Party Panel: Putting the evidence in evidence-based nightlife prevention"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-05-16T09:30:00
date_end = 2019-05-16T10:00:00
all_day = false

# Schedule page publish date (NOT talk date).
publishDate = 2019-01-01T12:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Gjalt-Jorn Peters"]

# Location of event.
location = "Amsterdam, The Netherlands"

# Name of event and optional event URL.
event = "Club Health Conference"
event_url = "https://clubhealthconference.com"

# Abstract. What's your talk about?
abstract = "Like other forms of recreation. nightlife participation has numerous benefits but also entails a number of risks. This justifies prevention efforts to reduce the associated harms, and to be effective, such prevention efforts need to be based in theory and evidence. Party Panel is an annual semi-panel study among Dutch nightlife patrons where each year, the determinants of a different nightlife-related risk behavior are studied. The data on these determinants and sub-determinants are then analysed using confidence-interval based estimation of relevance (CIBER) plots and reported back to prevention workers to make this evidence available to inform intervention development, and data and other resources are made publicly available. This contribution will consist of three parts. First, the design of Party Panel will be discussed. Second, a selection of the results of the first four rounds on using highly dosed ecstasy; visiting first-aid in  nightlife settings, using earplugs, and sexual boundaries will be presented. Third, pointers will be provided on how these results can be leveraged in the intervention development process using acyclic behavior change diagrams. Together, this workflow can help enhance the quality and thereby effectiveness of prevention efforts. As all materials are public, this workflow can feasibly be adapted to other countries."

# Summary. An optional shortened abstract.
summary = "An illustration of how Party Panel help to achieve effective behavior change in nightlife settings."

# Is this a featured talk? (true/false)
featured = true

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Behavior Change", "Party Panel"]

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional filename of your slides within your talk folder or a URL.
url_slides = "2019--GJY-Peters--Party-Panel--Club-Health.pdf"

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Links (optional).
url_pdf = ""
url_video = ""
url_code = ""

# Demo talk page uses LaTeX math.
math = false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

## Referenced articles:

- Peters (2014) Practical guide: how to identify what to change, https://doi.org/10.31234/osf.io/hy7mj
- Crutzen & Peters (2017) Evolutionary learning processes, https://doi.org/10.1080/17437199.2017.1362569
- Kok et al. (2018) Behavior Change Methods, https://doi.org/10.1080/17437199.2015.1077155
- Peters & Crutzen (2015) Pragmatic Nihilism, https://doi.org/10.1080/17437199.2015.1077155
- Peters & Noijen (2019) Party Panel design, https://doi.org/10.31234/osf.io/akpmx

## Party panel data and resources

- https://gitlab.com/partypanel

## Acyclic Behavior Change Diagrams

- The example ABCD table at Google Docs: https://docs.google.com/spreadsheets/d/1ItMFZTef1aPi_EzEWST2fushSBHA3Wi0YhSWD2y-XsM/edit#gid=0
- The app to generate ABCDs is located at https://a-bc.eu/apps/abcd
- More explanations are available at https://a-bc.eu/abcd

## Confidence Interval-Based Estimation of Relevance (CIBER) plots

- https://a-bc.gitlab.io/website/tutorial/ciber


<!--

{{% alert note %}}
Click on the **Slides** button above to view the built-in slides feature.
{{% /alert %}}

Slides can be added in a few ways:

- **Create** slides using Academic's *Slides* feature and link using `url_slides` parameter in the front matter of the talk file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the talk file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://sourcethemes.com/academic/docs/writing-markdown-latex/).

Further talk details can easily be added to this page using *Markdown* and $\rm \LaTeX$ math code.

-->
