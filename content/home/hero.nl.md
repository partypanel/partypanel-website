+++
# Hero widget.
widget = "hero"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "Welkom"

# Hero image (optional). Enter filename of an image in the `static/img/` folder.
hero_media = "partypanel-logo-huge.png"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "white"
  
  # Background gradient.
  # gradient_start = "#4bb4e3"
  # gradient_end = "#2b94c3"
  
  # Background image.
  image = "party-people-1644798.jpg"  # Name of image in `static/img/`.
  image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = true

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `[cta]`.
#   Remove a link/note by deleting a cta/note block.
#[cta]
#  url = "tutorial/"
#  label = "Neem een kijkje"
#  icon_pack = "fas"
#  icon = "download"
#  
#[cta_alt]
#  url = "https://sourcethemes.com/academic/"
#  label = "View Documentation"
#
# Note. An optional note to show underneath the links.
#[cta_note]
#  label = ""
#'<a id="academic-release" href="https://sourcethemes.com/academic/updates" data-repo="gcushen/hugo-academic">Latest release <!-- V --></a>'
+++
Doe mee met de eerste panel-studie in het uitgaansleven waarbij het niet gaat om wat je doet, maar waarom.
<!-- span style="color:#002f5eff">
Welkom
</span>
<!-- span style="text-shadow: none;"><a class="github-button" href="https://github.com/gcushen/hugo-academic" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star this on GitHub">Star</a><script async defer src="https://buttons.github.io/buttons.js"></script></span -->
