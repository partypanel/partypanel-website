+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Details"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  # image = "headers/ilight-2185506.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.8  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px;"
 
 # CSS class.
 css_class = ""
+++

Party Panel is een initatief van [Unity](https://unity.nl) en [Greater Good](https://greatergood.eu) dat is opgestart binnen [Project Party Safely](https://projectpartysafely.nl), waar ook de [Celebrate Safe campaign](https://celebratesafe.nl) campagne onder valt (zie de details voor meer informatie). Het doel is om op de hoogte te blijven van wat er speelt in het Nederlandse uitgaansleven. Deze informatie kunnen we dan gebruiken om goed te communiceren over gezond uitgaan.

# Algemene informatie

De vragenlijst start in juni 2015. Het doel is om in het begin twee keer per jaar en uiteindelijk vier keer per jaar een vragenlijst voor te leggen aan een panel aan onze deelnemers. Door middel van de unieke code die elke deelnemer zelf maakt kunnen we de gegevens koppelen terwijl de vragenlijst volledig anoniem blijft. Elke keer staat een ander onderwerp centraal (zie de resultaten), afhankelijk van de behoeften uit de praktijk. Deelnemers kunnen zelf ook suggesties doen op de laatste pagina van de vragenlijst.

# De resultaten

De uitkomsten van het onderzoek worden op drie manieren openbaar gemaakt:

- Ten eerste worden de resultaten gedeeld met de deelnemers van Party Panel. Als de resultaten zijn geanalyseerd, worden ze rondgestuurd naar iedereen die is aangemeld.
- Ten tweede worden de uitkomsten geanalyseerd en kijken experts in gedragsverandering van Greater Good er naar om adviezen te geven over communicatie over veilig uitgaan. Deze worden gecommuniceerd naar preventie-organisaties zoals Unity (zie voor meer informatie de details).
- Ten derde worden de analyses gerapporteerd in Open Access wetenschappelijke tijdschriften gepubliceerd, en de data (die immers volledig anoniem zijn), vragenlijsten, analyse-scripts, en output worden ook openbaar gemaakt. De data worden 2 jaar na de start van elke vragenlijst openbaar gemaakt (dus voor een vragenlijst die 1 juli 2015 start, worden de data 1 juli 2017 openbaar gemaakt).

# Anonimiteit

In een onderzoek als dit is anonimiteit erg belangrijk. Tegelijkertijd willen we deelnemers graag volgen over de tijd. Daarom gebruiken we de Party Panel Code. Dit is code die deelnemers makkelijk kunnen genereren via http://partypanel.nl?code. Met deze code is het niet mogelijk de identiteit van de deelnemers te herhalen. Bovendien hoeven de deelnemers niets te onthouden, terwijl het toch mogelijk is om de vragenlijsten van deelnemers aan elkaar te koppelen. Als iemand zich aanmeld voor Party Panel wordt het email adres opgeslagen in een aparte, beveiligde database. In deze database worden geen zogenaamde 'timestamps' of ip-adressen opgeslagen, zodat er geen enkele manier is waarop deze email adressen kunnen worden gekoppeld aan ingevulde vragenlijsten.


<!--{{% alert note %}}

{{% /alert %}}-->
