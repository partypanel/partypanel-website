+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Code"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  # image = "headers/ilight-2185506.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.8  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px;"
 
 # CSS class.
 css_class = ""
 js = ["ppcode", "ppcode.js"]
+++


<h1 class="cover-heading">Party Panel Code</h1>
<p class="lead">Hier kun je je unieke Party Panel Code genereren. Op die manier is je anonimiteit gegarandeerd!</p>
<p class="lead">
<form id="partyPanelCodeForm">
  <table style="width:80%;margin:0px auto;" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="135px;" ><label class="ppCode-label" for="firstname">Voornaam:</label></td>
      <td><input id="firstname" name="firstname" class="form-control ppCodeFormField" placeholder="Voornaam" autofocus /></td>
    </tr><tr>
      <td><label class="ppCode-label" for="lastname">Achternaam:</label></td>
      <td><input id="lastname" name="lastname" class="form-control ppCodeFormField" placeholder="Achternaam" /></td>
    </tr><tr>
      <td>
        <label class="ppCode-label" for="birthdate">Geboortedatum:</label>
      </td><td>
        <div class='input-group date ppCodeDateFix' id='birthdateDiv'>
          <input id="birthdate" type="text" name="birthdate" class="form-control ppCodeDateFix" placeholder="Geboortedatum" />
          <span class="input-group-addon ppCodeDateFix">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
      </td>
    </tr><tr>
      <td><label class="ppCode-label" for="birthplace">Geboorteplaats:</label></td>
      <td><input id="birthplace" name="birthplace" class="form-control ppCodeFormField" placeholder="Geboorteplaats" /></td>
    </tr><tr>
      <td><label class="ppCode-label" for="eyecolor">Oogkleur:</label></td>
      <td><select id="eyecolor" name="eyecolor" class="form-control ppCodeFormField" placeholder="Oogkleur">
            <option value='' selected disabled>Oogkleur</option>
            <option value='BLAUW'>Blauw</option>
            <option value='BRUIN'>Bruin</option>
            <option value='GROEN'>Groen</option>
            <option value='GRIJS'>Grijs</option>
          </select>
      </td>
    </tr>
  </table>
</form>
<!-- h1 class="cover-heading"><span class="text-muted">Jouw Code:</span><input id="partyPanelCode" class="form-control partyPanelCode text-important-neutral bg-important-neutral" readonly><span id="char1">-</span><span id="char2">-</span><span id="char3">-</span><span id="char4">-</span><span id="char5">-</span><span id="char6">-</span></input -->
<h1 class="cover-heading"><span id="partypanel-code-heading" class="text-muted">Jouw Code:</span><input id="partyPanelCode" class="form-control text-center partyPanelCode text-important-neutral bg-important-neutral" value="------" readonly ></input>
</h1>
<div class="alert alert-success hidden" id="ppcConfirm">
  <p>Dit is je Party Panel Code. Je kunt deze nu kopieren en plakken in de Party Panel vragenlijst.</p><p>(Je kunt de code kopieren door er op te klikken en dan op CTRL-C te drukken, of door met de rechter muisknop op de code te klikken en 'kopieer' of 'copy' te kiezen.)</p>
</div>
<p class="smallprint" style="margin-top:40px;">Met deze code kunnen we jouw vragenlijsten aan elkaar koppelen, zonder dat we je persoonsgegevens hebben. Zo kunnen we de deelnemers aan Party Panel volgen over de tijd, terwijl Party Panel toch volledig anoniem blijft. Met deze pagina kun je je unieke code altijd weer uitrekenen, zodat je niets hoeft te onthouden. Deze pagina communiceert daarom ook niet met het internet: de informatie die je ingeeft en de code die je ziet blijft in je browser (als je dit wil checken kun je de broncode van deze pagina bekijken door te rechtsklikken en dan 'view page source' te kiezen).</p>

<!-- JQuery -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<!-- Bootstrap -->

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- Bootstrap Date/time picker -->

<script type="text/javascript" src="../js/bootstrap-datetimepicker.min.js"></script>



<script type="text/javascript">
  $(document).ready(function () {
  
    String.prototype.removeDiacritics = function() {
      var diacritics = [
          [/[\300-\306]/g, 'A'],
          [/[\340-\346]/g, 'a'],
          [/[\310-\313]/g, 'E'],
          [/[\350-\353]/g, 'e'],
          [/[\314-\317]/g, 'I'],
          [/[\354-\357]/g, 'i'],
          [/[\322-\330]/g, 'O'],
          [/[\362-\370]/g, 'o'],
          [/[\331-\334]/g, 'U'],
          [/[\371-\374]/g, 'u'],
          [/[\321]/g, 'N'],
          [/[\361]/g, 'n'],
          [/[\307]/g, 'C'],
          [/[\347]/g, 'c'],
          [/[\377]/g, 'y'],
      ];
      var s = this;
      for (var i = 0; i < diacritics.length; i++) {
          s = s.replace(diacritics[i][0], diacritics[i][1]);
      }
      return s;
    }
  
    $('#birthdateDiv').datetimepicker({
      format: 'dd-mm-yyyy',
      startView: 4,
      initialDate: '1990-04-01',
      minView: 2,
      autoclose: true,
      pickerPosition: 'bottom-left'
    });

    $('#partyPanelCode').click(function(){
      $(this).blur();
    });
    
    function verifyPartyPanelCode() {
      var partyPanelCode = $('#partyPanelCode').val();
      if (partyPanelCode.match(/\w{6}/)) {
        $('#partyPanelCode').addClass('text-important-positive bg-important-positive');
        $('#partyPanelCode').removeClass('text-important-neutral bg-important-neutral');
        $('#partypanel-code-heading').removeClass('text-muted');
        $('#partyPanelCode').click(function(){
          $(this).select();
        });
        $('#ppcConfirm').removeClass('hidden');
      } else {
        $('#partyPanelCode').addClass('text-important-neutral bg-important-neutral');
        $('#partyPanelCode').removeClass('text-important-positive bg-important-positive');
        $('#partypanel-code-heading').addClass('text-muted');
        $('#partyPanelCode').click(function(){
          $(this).blur();
        });
        $('#ppcConfirm').addClass('hidden');
      }
    }
    
    function replacePartyPanelCodeCharacter(newChar, position) {
      position = position - 1;
      var oldCode = $('#partyPanelCode').val();
      var firstBit = oldCode.substr(0, position);
      var midBit = newChar;
      var lastBit = oldCode.substr(position+1, 6-position);
      var newCode = firstBit + midBit + lastBit;
      $('#partyPanelCode').val(newCode);
    }

    $('#firstname').on('change', function(){
      
      theWord = $('#firstname').val().removeDiacritics().toUpperCase();
      $('#firstname').val(theWord);
      //console.log(theWord);
      
      theWord = theWord.replace(/[^a-zA-Z]+/g, '');
      //console.log(theWord);
      
      var alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                      'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                      'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

      if (theWord.length > 0) {
        if (theWord.length == 1) {
          chosenCharacter = theWord;
        } else if (theWord.length > 1) {
          chosenCharacter = theWord.substr(1, 1);
        }
        chosenCharacter = alphabet[25-alphabet.indexOf(chosenCharacter)];
        replacePartyPanelCodeCharacter(chosenCharacter, 1);
        verifyPartyPanelCode();
      }
    });

    $('#lastname').on('change', function(){

      theWord = $('#lastname').val().removeDiacritics().toUpperCase();
      $('#lastname').val(theWord);

      theWord = theWord.replace(/[^a-zA-Z]+/g, '');
      //console.log(theWord);

      if ((theWord.length > 0) && (theWord.length < 4)) {
        replacePartyPanelCodeCharacter(theWord.substr(0, 1), 2);
        verifyPartyPanelCode();
      } else if (theWord.length >= 4) {
        replacePartyPanelCodeCharacter(theWord.substr(theWord.length - 4, 1), 2);
        verifyPartyPanelCode();
      }
    });

    $('#birthdate').on('change', function(){
      if ($('#birthdate').val().length > 9) {
        replacePartyPanelCodeCharacter($('#birthdate').val().substr(1, 1), 3);
        replacePartyPanelCodeCharacter($('#birthdate').val().substr(9, 1), 4);
        verifyPartyPanelCode();
      }
    });

    $('#birthplace').on('change', function(){

      theWord = $('#birthplace').val().removeDiacritics().toUpperCase();
      $('#birthplace').val(theWord);
      //console.log(theWord);

      theWord = theWord.replace(/[^a-zA-Z]+/g, '');
      //console.log(theWord);
      
      if (theWord.length == 1) {
        replacePartyPanelCodeCharacter(theWord, 5);
        verifyPartyPanelCode();
      } else if (theWord.length > 1) {
        replacePartyPanelCodeCharacter(theWord.substr(0, 1), 5);
        verifyPartyPanelCode();
      }
    });

    $('#eyecolor').on('change', function(){
      replacePartyPanelCodeCharacter($('#eyecolor').val().substr(2, 1).toUpperCase(), 6);
      verifyPartyPanelCode();
    });
    
   });
</script>


<!--{{% alert note %}}

{{% /alert %}}-->
