+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Results"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  image = "headers/ilight-2185506.jpg"  # Name of image in `static/img/`.
  image_darken = 0.8  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px;"
 
 # CSS class.
 css_class = ""
+++

Party Panel results are made public in four ways:

- In 2019, infographics will be produced for every wave, to inform participants of a selection of results.
- In 2019, brief movies will be produced for every wave, to present the results to prevention professionals.
- Detailed reports for each wave are generated and are available form the links below.
- Finally, all resources and products are [made public at GitLab](https://gitlab.com/partypanel).

## Reports

| Round | Dates                     | Topic                                      | Results                                                          |
|-------|---------------------------|--------------------------------------------|------------------------------------------------------------------|
| 15.1  | 2015-07-01 to 2016-01-01  | Highly dosed MDMA & getting XTC tested     | [Report](https://partypanel.nl/resultResources/15.1/report.html) |
| 16.1  | 2016-05-27 to 2016-10-01  | Visiting nightlife first-aid facilities    | [Report](https://partypanel.nl/resultResources/16.1/report.html) |
| 17.1  | 2017-04-20 to 2017-09-15  | Wearing earplugs to prevent hearing damage | [Report](https://partypanel.nl/resultResources/17.1/report.html) |
| 18.1  | 2018-07-04 to ...         | Boundary crossing when flirting            | [Report](https://partypanel.nl/resultResources/18.1/report.html) |

<!--{{% alert note %}}

{{% /alert %}}-->
