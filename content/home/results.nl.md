+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Resultaten"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  image = "headers/ilight-2185506.jpg"  # Name of image in `static/img/`.
  image_darken = 0.8  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px;"
 
 # CSS class.
 css_class = ""
+++

Party Panel resultaten worden openbaar gemaak via vier routes:

- In 2019 worden infographics geproduceerd voor elke ronde, om deelnemers te informeren over een selectie van de resultaten.
- In 2019 worden korte filmpjes geproduceerd voor elke ronde, om preventieprofessionals te informeren over de resultaten.
- Er worden gedetailleerde rapportages geproduceerd voor elke ronde; deze zijn hieronder beschikbaar.
- Tot slot worden alle producten en middelen [openbaar gemaakt via GitLab](https://gitlab.com/partypanel).

## Rapporten

| Ronde | Data                      | Onderwerp                                 | Resultaten                                                       |
|-------|---------------------------|-------------------------------------------|------------------------------------------------------------------|
| 15.1  | 2015-07-01 tot 2016-01-01 | Hooggedoseerde MDMA & XTC laten testen    | [Report](https://partypanel.nl/resultResources/15.1/report.html) |
| 16.1  | 2016-05-27 tot 2016-10-01 | Bezoek van de EHBO in het uitgaansleven   | [Report](https://partypanel.nl/resultResources/16.1/report.html) |
| 17.1  | 2017-04-20 tot 2017-09-15 | Oordopjes dragen tegen gehoorbeschadiging | [Report](https://partypanel.nl/resultResources/17.1/report.html) |
| 18.1  | 2018-07-04 tot ...        | Grensoverschrijding bij flirten           | [Report](https://partypanel.nl/resultResources/18.1/report.html) |

<!--{{% alert note %}}

{{% /alert %}}-->
