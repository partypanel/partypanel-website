+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Details"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  # image = "headers/ilight-2185506.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.8  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px;"
 
 # CSS class.
 css_class = ""
+++

Party Panel is an initiative by [Unity](https://unity.nl) and [Greater Good](https://greatergood.eu) that was initialised in [Project Party Safely](https://projectpartysafely.nl), which also manages the [Celebrate Safe campaign](https://celebratesafe.nl). The goal is to gather up-to-date insights in current nightlife trends. This information can then be used to communication properly about participating in the nightlife in a health manner.

# General information

This questionnaire starts in the summer of 2016. The goal is to eventually repeatedly release questionnaires, to map different nightlife aspects. By using a unique code that every participant generates themselves data of successive questionnaires can be linked while still maintaining complete participant anonimity. Every questionnaire will focus on a different subject, depending on the needs of nightlife professionals. Participants can also suggest subjects themselves, on the last page of the questionnaire.

# Results

The results of the questionnaire will be published in three ways:

- First, the results will be shared with the Party Panel members.
- Second, the results will be analysed and experts in behavior change from Greater Good will inspect them to formulate recommendations about enjoying the nightlife in a healthy manner. These will be communicated to prevention organisations such as Unity.
- Finally, the results will be reported in Open Access scientific journals, and the data (which are anonymous), questionnaire, analysis scripts, and output will be published as well. A two year ambargo rests on the data and other resources of every Party Panel wave.

# Anonimity

In a study such as this, anonimity is very important. At the same time we would like to be able to follow participants over time. To solve this, we use the Party Panel code. This is a code that participants can easily generate at http://partypanel.eu/code. This code does not allow deriving participants' identities. In addition, participants won't have to remember anything, while still enabling linking successive questionnaires. When somebody registers for Party Panel, their email address is stored in a separate, secured database. This database does not store any so-called 'timestamps' or ip-adresses, making sure there is no way to link these email addresses to questionnaire results.

<!--{{% alert note %}}

{{% /alert %}}-->
